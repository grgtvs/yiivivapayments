<?php

class m140205_221957_viva_transactions extends CDbMigration
{
	public function up()
	{
        $this->createTable('viva_transaction', array(
            'id'=>'pk',
            'user_id'=>'integer unsigned not null comment \'The user that made the transcation (CustomerTrns) \'',
            'order_id'=>'integer unsigned not null comment \'Our order code (TotalInstallments)\'',
            'order_code'=>'bigint(20) not null comment \'Viva\\\'s order code\'',
            'txn_id'=>'varchar(60) default null',
            'orig_txn_id'=>'varchar(60) default null',
            'status'=>'char(1) not null default \'A\'',
            'amount'=>'decimal(11,2) not null',
            'commission'=>'decimal(11,2) not null default 0',
            'txn_type'=>'tinyint(3) unsigned not null',
            'medium_details'=>'text',
            'recurring_support'=>'tinyint(1) unsigned not null comment \'If the txn id can be used for recurring payments.\'',
            'created_at'=>'datetime not null',
            'updated_at'=>'datetime not null',
            'INDEX idx_order_id_combo (user_id, order_id, order_code)',
            'INDEX idx_txn_id (txn_id)',
            'INDEX idx_orig_txn_id (orig_txn_id)'
        ), 'Engine=InnoDB AUTO_INCREMENT=1 CHARACTER SET=UTF8 DEFAULT COLLATE=utf8_general_ci');
	}

	public function down()
	{
        $this->dropTable('viva_transaction');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
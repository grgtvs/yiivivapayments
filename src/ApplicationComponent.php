<?php
namespace CodeJunkii\YiiVivaPayments;

use \CodeJunkii\VivaPayments\Client;

class ApplicationComponent extends \CApplicationComponent
{
    const ERR_INVALID_TXNID         = 1000;
    const ERR_INVALID_ORDERID       = 1001;
    const ERR_ALREADY_PROCESSED     = 1002;
    const ERR_AMOUNT_TAMPERED       = 1003;

    public $arClass         = 'VivaTransaction';
    public $merchantId;
    public $apiKey;
    public $environment     = \CodeJunkii\VivaPayments\Client::ENV_SANDBOX;
    /**
     * VivaPayments client options
     * @var array
     */
    public $options = array();

    /**
     * [$_errors description]
     * @var array
     */
    private $_errors = array();
    /**
     * [$_viva description]
     * @var [type]
     */
    private $_viva = null;

    /**
     * [init description]
     * @return [type] [description]
     */
    public function init()
    {
        parent::init();
        $this->_viva = new Client($this->environment, $this->merchantId, $this->apiKey);
        foreach ($this->options as $key => $value) {
            $this->_viva->{$key} = $value;
        }
    }

    /**
     * Confirms a transaction's validity
     *
     * @param  [type] $txnId [description]
     * @return [type]        [description]
     */
    public function confirmTransaction($orderCode, $txnId)
    {
        $txn = $this->getTransactionInfo($txnId);

        if ($txn===null){
            $this->_errors[] = self::ERR_INVALID_TXNID;
            return false;
        }

        if ($txn['Order']['OrderCode'] != $orderCode) {
            $this->_errors[] = self::ERR_INVALID_ORDERID;
            return false;
        }

        $orderId = $txn['MerchantTrns'];
        $trans = $this->getPersistent()->findByAttributes(array('order_id'=>$orderId));

        if ($trans->txn_id !== null || $trans->status !== \CodeJunkii\VivaPayments\Client::TXN_STATUS_UNDERWAY) {
            $this->_errors[] = self::ERR_ALREADY_PROCESSED;
            return false;
        }

        if (round($trans->amount, 2) != round($txn['Amount'], 2)) {
            $this->_errors[] = self::ERR_AMOUNT_TAMPERED;
            return false;
        }

        $this->onTransctionConfirmed(new \CEvent($this, array('txnInfo'=>$txn, 'txn'=>$trans)));

        return true;
    }

    /**
     * [getErrors description]
     * @return [type] [description]
     */
    public function getErrors()
    {
        return array_merge($this->_errors, array($this->_viva->error));
    }

    ## Events Starts
    /**
     * [onAfterAuthorize description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function onAfterAuthorize(\CEvent $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onBeforeAuthorize description]
     * @param  Event $event [description]
     * @return [type]        [description]
     */
    public function onBeforeAuthorize(Event $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onBeforeCreateOrder description]
     * @param  Event $event [description]
     * @return [type]        [description]
     */
    public function onAfterCreateOrder(\CEvent $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onBeforeCreateOrder description]
     * @param  Event $event [description]
     * @return [type]        [description]
     */
    public function onBeforeCreateOrder(Event $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }

    /**
     * [onAfterCreateRecurringTransaction description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function onAfterCreateRecurringTransaction(\CEvent $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onAfterCreateRecurringTransaction description]
     * @param  Event $event [description]
     * @return [type]        [description]
     */
    public function onBeforeCreateRecurringTransaction(Event $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onAfterCancelOrder description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function onAfterCancelOrder(\CEvent $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onBeforeCancelOder description]
     * @param  Event $event [description]
     * @return [type]        [description]
     */
    public function onBeforeCancelOrder(Event $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onAfterCancelTransaction description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function onAfterCancelTransaction(\CEvent $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onBeforeCancelTransaction description]
     * @param  Event $event [description]
     * @return [type]        [description]
     */
    public function onBeforeCancelTransaction(Event $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    /**
     * [onTransctionConfirmed description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function onTransctionConfirmed(\CEvent $event)
    {
        $this->raiseEvent(__FUNCTION__, $event);
    }
    ## Events End

    ## Protected / Private methods Start

    /**
     * Just notifies any handlers about the method result
     *
     * @param  string $method The called method
     * @param  array $params The params passed at the method call
     * @param  mixed $result The outcome of that call.
     * @return void
     */
    protected function afterMethod($method, $params, &$result)
    {
        $event = new \CEvent($this, array('result'=>$result, 'method'=>$method, 'params'=>$params));
        $eventMethod = "onAfter{$method}";
        $this->{$eventMethod}($event);
    }

    /**
     * Flow control event.
     *
     * @param  sting $method  The viva api method being called
     * @param  array  $params Any params passed by
     * @return boolean        If the process should continue or not.
     */
    protected function beforeMethod($method, $params=array())
    {
        $event = new Event($this, array('method'=>$method, 'params'=>$params));
        $eventMethod = "onBefore{$method}";
        $this->{$eventMethod}($event);
        return $event->getIsInterupted() ? FALSE : TRUE;
    }

    /**
     * Return a CActiveRecord Model
     * @return YVivaTransaction The signeton of the YVivaTransaction
     */
    protected function getPersistent()
    {
        return \CActiveRecord::model($this->arClass);
    }

    ## Protected / Private methods End

    ## Magic methods - Start
    /**
     * [__call description]
     * @param  [type] $method [description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function __call($method, $params)
    {
        if (method_exists($this->_viva, $method)) {
            if (in_array($method, self::flowControlledMethods())) {
                if($this->beforeMethod($method, $params)){
                    $result = call_user_func_array(array($this->_viva, $method), $params);
                    // this method can alter the result returned
                    $this->afterMethod($method, $params, $result);
                    return $result;
                }
                return false;
            }

            return call_user_func_array(array($this->_viva, $method), $params);
        }
        return parent::__call($method, $params);
    }

    ## Magic methods - End

    ## Static Methods Start
    /**
     * [flowControlledMethods description]
     * @return [type] [description]
     */
    private static function flowControlledMethods()
    {
        return array(
            'authorize',
            'createOrder',
            'createRecurringTransaction',
            'cancelOrder',
            'cancelTransaction',
            'refund',
        );
    }
    ## Static Methods End
}

<?php
namespace CodeJunkii\YiiVivaPayments;

/**
 * Abstract class as the base for every YVivaPayments Event handler
 */
abstract class EventHandler extends \CBehavior {
    /**
     * [events description]
     * @return [type] [description]
     */
    public function events()
    {
        return array(
            'onAfterAuthorize'                  => 'afterAuthorize',
            'onBeforeAuthorize'                 => 'beforeAuthorize',
            'onAfterCreateOrder'                => 'afterCreateOrder',
            'onBeforeCreateOrder'               => 'beforeCreateOrder',
            'onAfterCreateRecurringTransaction' => 'afterCreateRecurringTransaction',
            'onBeforeCreateRecurringTransaction'=> 'beforeCreateRecurringTransaction',
            'onAfterCancelOrder'                => 'afterCancelOrder',
            'onBeforeCancelOrder'               => 'beforeCancelOrder',
            'onAfterCancelTransaction'          => 'afterCancelTransaction',
            'onBeforeCancelTransaction'         => 'beforeCancelTransaction',
        );
    }
    /**
     * [afterAuthorize description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function afterAuthorize(\CEvent $event){}
    /**
     * [beforeAuthorize description]
     * @param  YEvent $event [description]
     * @return [type]        [description]
     */
    public function beforeAuthorize(YEvent $event){}
    /**
     * [afterCreateOder description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function afterCreateOrder(\CEvent $event){}
    /**
     * [beforeCreateOder description]
     * @param  YEvent $event [description]
     * @return [type]        [description]
     */
    public function beforeCreateOrder(YEvent $event){}
    /**
     * [afterCreateRecurringTransaction description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function afterCreateRecurringTransaction(\CEvent $event){}
    /**
     * [beforeCreateRecurringTransaction description]
     * @param  YEvent $event [description]
     * @return [type]        [description]
     */
    public function beforeCreateRecurringTransaction(YEvent $event){}
    /**
     * [afterCancelOrder description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function afterCancelOrder(\CEvent $event){}
    /**
     * [beforeCancelOrder description]
     * @param  YEvent $event [description]
     * @return [type]        [description]
     */
    public function beforeCancelOrder(YEvent $event){}
    /**
     * [afterCancelTransaction description]
     * @param  CEvent $event [description]
     * @return [type]        [description]
     */
    public function afterCancelTransaction(\CEvent $event){}
    /**
     * [beforeCancelTransaction description]
     * @param  YEvent $event [description]
     * @return [type]        [description]
     */
    public function beforeCancelTransaction(YEvent $event){}
}

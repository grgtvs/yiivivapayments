<?php
namespace CodeJunkii\YiiVivaPayments;

/**
 * This is the model class for table "viva_transaction".
 *
 * The followings are the available columns in table 'viva_transaction':
 * @property integer $id
 * @property string $user_id
 * @property string $order_id
 * @property string $order_code
 * @property string $txn_id
 * @property string $orig_txn_id
 * @property string $status
 * @property string $amount
 * @property string $commission
 * @property integer $txn_type
 * @property string $medium_details
 * @property integer $recurring_support
 * @property string $created_at
 * @property string $updated_at
 */
class Transaction extends \CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'viva_transaction';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, order_id, order_code, amount', 'required', 'on'=>'create,insert'),
            array('user_id, order_id, order_code, amount, txn_type, recurring_support', 'required', 'on'=>'update'),
            array('txn_type, recurring_support', 'numerical', 'integerOnly'=>true),
            array('user_id, order_id, amount, commission', 'length', 'max'=>11),
            array('order_code', 'length', 'max'=>20),
            array('txn_id, orig_txn_id', 'length', 'max'=>60),
            array('status', 'length', 'max'=>1),
            array('medium_details', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, order_id, order_code, txn_id, orig_txn_id, status, amount, commission, txn_type, medium_details, recurring_support, created_at, updated_at', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'The user that made the transcation (CustomerTrns) ',
            'order_id' => 'Our order code (TotalInstallments)',
            'order_code' => 'Viva\'s order code',
            'txn_id' => 'Txn',
            'orig_txn_id' => 'Orig Txn',
            'status' => 'Status',
            'amount' => 'Amount',
            'commission' => 'Commission',
            'txn_type' => 'Txn Type',
            'medium_details' => 'Medium Details',
            'recurring_support' => 'If the txn id can be used for recurring payments.',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id,true);
        $criteria->compare('order_id',$this->order_id,true);
        $criteria->compare('order_code',$this->order_code,true);
        $criteria->compare('txn_id',$this->txn_id,true);
        $criteria->compare('orig_txn_id',$this->orig_txn_id,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('amount',$this->amount,true);
        $criteria->compare('commission',$this->commission,true);
        $criteria->compare('txn_type',$this->txn_type);
        $criteria->compare('medium_details',$this->medium_details,true);
        $criteria->compare('recurring_support',$this->recurring_support);
        $criteria->compare('created_at',$this->created_at,true);
        $criteria->compare('updated_at',$this->updated_at,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return VivaTransaction the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}

<?php
namespace CodeJunkii\YiiVivaPayments;

class Event extends \CEvent
{
    /**
     * A flag that indicates the exit value of the chained events
     * TRUE means that a the event chain has been interupted and no further
     * processing should take place.
     * Has meaning on flow control event like this before an action
     * @var boolean
     */
    private $_result = FALSE;

    /**
     * Interrupt or not a event chain
     * @param boolean Strict only boolean affects the flow
     */
    public function setIsInterupted($boolean)
    {
        if ($boolean === TRUE) {
            $this->handled = TRUE;
            $this->_result = TRUE;
        }

        $this->_result = FALSE;
    }

    /**
     * Returns the value if the event chain has been interupted
     * @return boolean
     */
    public function getIsInterupted()
    {
        return $this->_result;
    }
}